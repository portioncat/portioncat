# -*- coding: utf-8 -*-

from gluon.scheduler import Scheduler

## initializes sendsms function in message.py
def send(phone,sched_id):
    URL('message','sendsms',vars=dict(phone=phone,sched_id=sched_id))

#### WARNING: Scheduler is not working properly ####
## initialize scheduler tables. add send_init function
scheduler = Scheduler(db, tasks=dict(send=send))


## cat stores the profiles of all the user's cats
db.define_table('cats',
                Field('name','string',requires=IS_NOT_EMPTY()),
                Field('weight','double',requires=IS_NOT_EMPTY()),
                Field('pref_food','string',requires=IS_NOT_EMPTY()),
                Field('picture','upload'),
                auth.signature)

## calcportion stores portion that should be given to cat for
## both wet and dry foods
db.define_table('calcportion',
                Field('wet','double'),
                Field('dry','double'),
                Field('cats_id','reference cats'))

## schedule will be used to notify user that it is time to feed their cat
db.define_table('schedules',
                Field('fed_cat','boolean'),
                Field('feeding_time','time',requires=[IS_NOT_EMPTY(),
                                                      IS_TIME(error_message='Must be 24 hour clock format HH:MM:SS (e.g. Enter 14:25:00 for 2:25 PM).')
                                                     ]),
                auth.signature)
