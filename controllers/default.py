# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################


def index():
    if auth.is_logged_in():
        redirect(URL('mycat'))
    welcomeMessage = T('Welcome ! Please  log in')
    return dict(welcomeMessage=welcomeMessage)

def about():
    return dict()

def tour():
    return dict()

def getstarted():
    response.flash = "Welcome"
    form = SQLFORM(db.cats, buttons = [ A("Save Changes",_class='btn',_href=URL("default","mycat"))])
    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)

@auth.requires_login() 
def addcat():
    db.cats.picture.requires = IS_IMAGE()
    form = SQLFORM(db.cats).process()
    if form.accepted:
        #response.flash = 'form accepted'
        weight = float(form.vars.weight)
        dry = calculateDry(weight)
        wet = calculateWet(weight)
        db.calcportion.insert(wet=wet,dry=dry,cats_id=form.vars.id)
        redirect(URL('mycat'))
    elif form.errors:
        response.flash = 'The form has errors.'
    return dict(form=form)

@auth.requires_login() 
#when people go to this page they have to be logged in
def mycat():
    mycats = db(db.cats.created_by == auth.user_id).select()
    #needs to return cats that are submitted by the user
    return dict(mycats=mycats)

@auth.requires_login() 
def deleteCat():
    cat = request.args[0]
    db(db.cats.name == cat).delete()
    redirect(URL('mycat'))

@auth.requires_login() 
def catprofile():
    delete_cat = request.args[0]
    req = request.args[0].replace("%20"," ")
    thiscat = db(db.cats.name == req).select()
    portion = None
    for row in thiscat:
        portion = db(db.calcportion.cats_id == row).select()
    return dict(thiscat=thiscat, portion=portion)


def calculateDry(weight):
    calories = 30.00*weight
    portionInCups = calories/300.00
    return portionInCups

def calculateWet(weight):
    calories = 30.00*weight
    portionInOz  = (calories/250.00)*6
    return portionInOz


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """

    auth.settings.register_onaccept.append(lambda f: f.vars.send_sms_reminders_to_feed_cat if redirect(URL('message','scheduler')) else redirect(URL('mycats')))
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
