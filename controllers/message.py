# -*- coding: utf-8 -*-

# WhitePages API Wrapper provided by Michal Monselise from https://github.com/michalmonselise/whitepages
# WhitePages API Wrapper sends a GET request to the White Pages server in order to
# do a reverse phone lookup. The server responds with a JSON file of information attached
# to that number, which has been parsed by the api wrapper.

import time
import datetime
from whitepages import WhitePages
from gluon.contrib.sms_utils import SMSCODES, sms_email
from gluon.scheduler import Scheduler


## takes a datetime.time object as form input in message/scheduler.html to schedule daily text message
## reminders to feed cat their daily portions. If form is accepted it will perform calculations to see
## if user is scheduling time starting today or tomorrow. When the calculations are done the task is added
## to scheduler_task database where it will perform the task (send sms reminders) in the background.
@auth.requires_login()
def scheduler():
    form = SQLFORM(db.schedules).process()
    row = db(db.schedules.created_by == auth.user_id).select()
    phone = next(r.phone for r in db(db.auth_user.id == auth.user_id).select())
    if form.accepted:
        dt = request.now
        userdt = datetime.datetime.combine(datetime.date.today(), form.vars.feeding_time)
        time_diff = (userdt-dt).total_seconds()
        day_sec = 3600 * 24
        if time_diff < 0:
            tommorrow = datetime.date.today() + datetime.timedelta(days=1)
            sched_time = datetime.datetime.combine(tommorrow, form.vars.feeding_time)
            db.scheduler_task.insert(function_name='send', task_name='send',
                                     vars=dict(phone=phone,sched_id=form.vars.id),
                                     start_time=sched_time, period=day_sec, repeats=0)
            response.flash = 'scheduled!'
        elif time_diff > 0:
            db.scheduler_task.insert(function_name='send', task_name='send',
                                     vars=dict(phone=phone,sched_id=form.vars.id),
                                     start_time=userdt, period=day_sec, repeats=0)
            response.flash = 'scheduled!'
        else:
            db.scheduler_task.insert(function_name='send', task_name='send',
                                     vars=dict(phone=phone,sched_id=form.vars.id),
                                     start_time=userdt, period=day_sec, repeats=0)
            response.flash = 'scheduled!'
    elif form.errors:
        response.flash = 'invalid schedule'
    return dict(schedule=form, row=row)

## deletes schedule from db.schedules
## task in scheduler_task must be deleted manually
@auth.requires_login()
def deleteSchedule():
    db(db.schedules.id == request.args[0]).delete()
    session.flash = 'Reminder deleted'
    redirect(URL('message','scheduler'))

## updates fed_cat field in db.schedules once user has clicked on
## link sent through sms
def catfed():
    sched_id = request.vars.sched_id
    db(db.schedules.id == sched_id).update(fed_cat=True)
    session.flash = 'Cat has been fed!'
    redirect(URL('default','index'))


### WARNING: getprovider() does not work on pythonanywhere server. Run application locally for appropriate results. ###
# getprovider() uses the WhitePages API Wrapper to get the carrier of a given phone number.
def getprovider(phone):
    url = "https://proapi.whitepages.com/2.2/phone.json?"
    phone_num = phone
    api_key = '7f014e0df08609aa49b7b5b6bb76e7e9'
    w = WhitePages(api_key)
    result = w.phone(phone_number=phone_num)
    return result[0].carrier

## format phone number to be sent through sms_email function in sendsms
def formatnumber(phone):
    phonef = '1 (%s%s%s) %s%s%s-%s%s%s%s' % tuple(phone)
    return phonef

## sends a sms message through phone providers email service
## sends link confirming that cat has been fed for the day
def sendsms():
    phone = request.vars.phone
    sched_id = request.vars.sched_id
    if phone != None and sched_id != None:
        phonef = formatnumber(phone)
        provider = getprovider(phone)
        sms_code = next(k for k in SMSCODES if provider in k)
        email = sms_email(phonef,sms_code)
        url = URL('message','catfed', vars=dict(sched_id=sched_id), scheme=True)
        mail.send(to=email, subject='Has your cat had their portion today?', message='If so click on the link: ' + url)
