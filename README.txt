# PortionCat #

## Contributors ##
* Jocelynne Gonzales
* Christian Loera
* Anish Krishnan

## Overview ##
PortionCat is a web application that will allow users to create profiles, upload information about their cats, and receive information about their recommended diet, as well as notifications to feed them.

PortionCat was originally designed in web2py by Jocelynne Gonzales and Christian Loera, and is being redesigned in Django by Anish Krishnan.


## Notes ##

### web2py ###
A few warnings about the web app's functionality: The scheduler.html page does not work as intended.  
We got the SMS system to work, but we weren't able to get the Web2py Scheduler working. Thus our automated SMS reminder system does not work.  

Please look at code and comments to see our intentions for that piece of functionality.  

Everything else should work as intended.
Visit our tour.html page to get a glimpse of what our web app has to offer. 



Here is link to our website hosted on pythonanywhere


https://portioncat.pythonanywhere.com/portioncat/default/index

### Django ###
The app is still in the pre-alpha process of design, but new elements have nonetheless been added, or are being added. Some new features include:
* The ability to select diseases your cat(s) may have
* The ability to select among a variety of different breeds
* The ability to create and view a user profile